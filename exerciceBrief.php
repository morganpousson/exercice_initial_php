<?php

echo "Exercice 0 <br>";

$var0 = 5;
echo $var0;
echo "<br><br>";

echo "Exercice 1 <br>";
$word = "c'est ";
$word2 = "vie.";
$word3 = "Simplon";
$word4 = "la";
echo $word3 . " ". $word . " " . $word4 . " ". $word2 . "<br><br>";

echo "Exercice 2 <br>";
$variable1=12;
$variable2=4;
echo $variable1 / $variable2;
echo "<br> <br>";

echo "Exercice 3 <br>";
$var="clement";
$var1= "extrapolation";
echo strlen($var . $var1);
echo "<br> <br>";

echo "Exercice 4 <br>";
$maîtrise_du_code = 1000;

if ( $maîtrise_du_code>= 1000 ) {
    echo "je maîtrise tellement le code maintenant";
}
echo "<br><br>";

echo "Exercice 5 <br>";
$chiffre_fetiche_sandrine = 7;
$chiffre_fetiche_xavier = 130;
$chiffre_fetiche_andre = 8;

$chiffre_fetiche_sandrine = $chiffre_fetiche_sandrine + $chiffre_fetiche_xavier;
//Sandrine = 137
$chiffre_fetiche_xavier = $chiffre_fetiche_sandrine - $chiffre_fetiche_xavier;
//Xavier = 7
$chiffre_fetiche_sandrine = $chiffre_fetiche_sandrine - $chiffre_fetiche_xavier;
//Sandrine = 130

echo "Le chiffre fétiche de Sandrine est de " . $chiffre_fetiche_sandrine . " <br>";
echo "Le chiffre fétiche de Xavier est de " . $chiffre_fetiche_xavier . " <br>";

$resultat = $chiffre_fetiche_andre.$chiffre_fetiche_xavier;
$soustraction = $chiffre_fetiche_sandrine - $resultat;
if ($soustraction < 50) {
    echo $soustraction." <br><br>";
} 

echo "Exercice 6 <br>";
$compte = -100;
if ($compte > 0) {
    echo "bravo, vous êtes un bon gestionnaire";
} elseif ($compte < 0) {
    echo "Vous faites vraiment n'importe quoi avec votre argent <br><br>";
}

echo "Exercice 7 <br>";

$vent = rand(15, 35);
$houle = rand (10, 35);
$cadence_vague = rand (5, 13);

if ($vent >= 30) {
    if ($houle < 20) {
        if ($cadence_vague >= 10) {
            echo "on peut surfer, les conditions sont bonnes ";

        } else echo "on ne peut pas surfer";
    } else echo "on ne peut pas surfer";
} 
elseif ($vent <30) {
    if ($houle <30) {
        if ($cadence_vague <= 8) {
                echo "on peut surfer, les conditions sont bonnes";

        } else echo "on ne peut pas surfer";
    } else echo "on ne peut pas surfer";
}

echo "<br> <br> Exercice 8 <br>";
$nombre_1 = 88;
$nombre_2 = 7;
$nombre_3 = 23;
$nombre_4 = 5;
$nombre_5 = 45;
$nombre_6 = 12;

if ($nombre_1 % 2 == 0) {
    echo "Le nombre ". $nombre_1 . " est un multiple de 2<br>";
} else echo "Le nombre ". $nombre_1 . " n'est pas un multiple de 2<br>";

if ($nombre_2 % 2 == 0) {
    echo "Le nombre ". $nombre_2 . " est un multiple de 2<br>";
} else echo "Le nombre ". $nombre_2 . " n'est pas un multiple de 2<br>";

if ($nombre_3 % 2 == 0) {
    echo "Le nombre ". $nombre_3 . " est un multiple de 2<br>";
} else echo "Le nombre ". $nombre_3 . " n'est pas un multiple de 2<br>";

if ($nombre_4 % 2 == 0) {
    echo "Le nombre ". $nombre_4 . " est un multiple de 2<br>";
} else echo "Le nombre ". $nombre_4 . " n'est pas un multiple de 2<br>";

if ($nombre_5 % 2 == 0) {
    echo "Le nombre ". $nombre_5 . " est un multiple de 2<br>";
} else echo "Le nombre ". $nombre_5 . " n'est pas un multiple de 2<br>";

if ($nombre_6 % 2 == 0) {
    echo "Le nombre ". $nombre_6 . " est un multiple de 2<br><br>";
} else echo "Le nombre ". $nombre_6 . " n'est pas un multiple de 2<br><br>";

echo "La soustraction des nombres 7, 23, 5, 45 au nombre 88 est égale à  ";
echo $nombre_1 - ($nombre_2 + $nombre_3 + $nombre_4 + $nombre_5) . ".<br>";

echo "La soustraction des nombres 7, 23, 5, 45 au nombre 12 est égale à  ";
echo $nombre_6 - ($nombre_2 + $nombre_3 + $nombre_4 + $nombre_5) . ".";